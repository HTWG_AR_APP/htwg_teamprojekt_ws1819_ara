package com.htwg.teamproject.arateamproject.directionControl;

import com.htwg.teamproject.arateamproject.graphEssentials.Node;

import java.util.ArrayList;
import java.util.List;

public class DirectionRelations {


    private List<Relation> relations = new ArrayList<>();

    public void generateRelationBetween(Node startNode, Node neighbourNode, String directionKey) {
        relations.add(new Relation(startNode, neighbourNode, new Direction(directionKey)));
    }

    public void generateRelationBetweenMultiDirection(Node startNode, Node neighbourNode, String firstDirectionKey, String oppositeDirectionKey) {
        relations.add(new Relation(startNode, neighbourNode, new Direction(firstDirectionKey)));
        relations.add(new Relation(neighbourNode, startNode, new Direction(oppositeDirectionKey)));
    }

    public Relation getRelationfor(Node startNode, Node neighbourNode) throws NoNeighbourNodeFoundExeption {
        for (Relation relation : relations) {
            if (startNode.getName().equals(relation.getStartNode().getName()) && neighbourNode.getName().equals(relation.getEndNode().getName())) {
                return relation;
            }
        }
        throw new NoNeighbourNodeFoundExeption("Not able to find neighbour node. Check NodeRelations");
    }


}
