package com.htwg.teamproject.arateamproject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.htwg.teamproject.arateamproject.helper.FetchTimeTablesRunnable;
import com.htwg.teamproject.arateamproject.helper.TimeTableProviderContainer;
import com.htwg.teamproject.arateamproject.pathFinding.PathFindingConstants;

import java.util.Timer;
import java.util.TimerTask;

public class StartScreenActivity extends AppCompatActivity implements LiveData {


    public static final String storageConnectionString =
            "DefaultEndpointsProtocol=https;" +
                    "AccountName=arappstorage;" +
                    "AccountKey=APBU1uVip9560ZzWwfsUh0TPV6E+J58n3Ajb/kUW8Bw/45ho3DPJH/RpZWUiqRucfSbYxr7hYpSFR7iuc+P+sQ==";
    StorageActivity st = (StorageActivity) new StorageActivity(this).execute();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_screen);

        TimeTableProviderContainer ttpr = new TimeTableProviderContainer();
        Thread t = new Thread(new FetchTimeTablesRunnable(ttpr));
        t.start();


        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    t.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Intent intent = new Intent(getApplicationContext(), IntroStartActivity.class);
                intent.putExtra("ttpr", ttpr);
                startActivity(intent);
            }
        }, 10);
    }


    @Override
    public void processFinish(String[] output) {
        if (Integer.parseInt(output[2]) <= Integer.parseInt(PathFindingConstants.airFix)) {
            PathFindingConstants.freshAirRoom = output[4];
        }
        SharedPreferences shared = this.getSharedPreferences("data", 0);
        SharedPreferences.Editor ed = shared.edit();
        ed.putString("device", output[0]);
        ed.putString("noise", output[1]);
        ed.putString("voc", output[2]);
        ed.putString("occurency", output[3]);
        ed.putString("room", output[4]);
        ed.commit();
    }

    @Override
    public void onBackPressed() {
        showToast("waiting to finish background-tasks");
    }


    private void showToast(String txt) {
        Toast.makeText(StartScreenActivity.this, txt, Toast.LENGTH_LONG).show();
    }


}
