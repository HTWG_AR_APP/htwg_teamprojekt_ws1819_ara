package com.htwg.teamproject.arateamproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.GridLayout;

import android.widget.Toast;


public class HomeScreenActivity extends AppCompatActivity {

    GridLayout mainGrid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home_screen);

        mainGrid = (GridLayout) findViewById(R.id.mainGrid);

        setSingleEvent(mainGrid);


    }

    private void setSingleEvent(GridLayout mainGrid) {

        for (int i = 0; i < mainGrid.getChildCount(); i++) {
            CardView cardView = (CardView) mainGrid.getChildAt(i);
            final int finalI = i;

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //       Toast.makeText(HomeScreenActivity.this, "Clicked at index" + finalI, Toast.LENGTH_SHORT).show();

                    if (finalI == 0) {    // Acitvity von Argmuented Reality
                        Intent intent = new Intent(HomeScreenActivity.this, TextRecognitionActivity.class);
//                        intent.putExtra("ttpr", getIntent().getSerializableExtra("ttpr"));
                        putTTPRInIntent(intent);
                        startActivity(intent);
                    } else if (finalI == 1) {    // Activity von Roominformation
                        Intent intent = new Intent(HomeScreenActivity.this, RoomInformationActivity.class);
                        putTTPRInIntent(intent);
                        startActivity(intent);
                    } else if (finalI == 2) {    // Activity von Introduction
                        Intent intent = new Intent(HomeScreenActivity.this, IntroductionActivity.class);
                        putTTPRInIntent(intent);
                        startActivity(intent);
                    } else if (finalI == 3) {    // Activity von About Us
                        Intent intent = new Intent(HomeScreenActivity.this, AboutUsActivity.class);
                        putTTPRInIntent(intent);
                        startActivity(intent);
                    } else if (finalI == 4) {    // Activity von Debug
                        Intent intent = new Intent(HomeScreenActivity.this, DebugViewActivity.class);
                        putTTPRInIntent(intent);
                        startActivity(intent);
                    } else {
                        Toast.makeText(HomeScreenActivity.this, "No activity found for this card item" + finalI, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }


    private void putTTPRInIntent(Intent intent) {
        intent.putExtra("ttpr", getIntent().getSerializableExtra("ttpr"));
    }


    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);
    }



  /*  public void loadSlides(View view) {

        new PreferenceManager(this).clearPreferences();
        startActivity(new Intent(this, StartScreenActivity.class));
        finish();
    }   */

}
