package com.htwg.teamproject.arateamproject;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;


public class RoomInformationActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private SlideAdapterActivity sliderAdapter;
    TextView text;

    Dialog myDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_information);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        sliderAdapter = new SlideAdapterActivity(this);
        viewPager.setAdapter(sliderAdapter);

        myDialog = new Dialog(this);
    }

    public void showPopUp(View view) {
        TextView textclose;
        myDialog.setContentView(R.layout.room_one_popup);
        textclose = (TextView) myDialog.findViewById(R.id.txtClose);
        textclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(RoomInformationActivity.this, HomeScreenActivity.class);
        intent.putExtra("ttpr", getIntent().getSerializableExtra("ttpr"));
        startActivity(intent);
    }


}
