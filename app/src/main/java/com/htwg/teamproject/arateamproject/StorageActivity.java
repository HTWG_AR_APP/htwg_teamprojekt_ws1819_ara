package com.htwg.teamproject.arateamproject;


import android.os.AsyncTask;
import android.util.Log;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlobDirectory;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import com.microsoft.azure.storage.blob.ListBlobItem;

import java.net.URISyntaxException;
import java.security.InvalidKeyException;

public class StorageActivity extends AsyncTask<String, Void, String[]> {
    private String deviceID;
    private String noise;
    private String voc;
    private String occurency;
    private String room;
    LiveData live;

    public StorageActivity(LiveData live) {
        this.live = live;
    }


    @Override
    protected String[] doInBackground(String... strings) {
        String[] result = new String[5];
        try {
            // Retrieve storage account from connection-string.
            CloudStorageAccount storageAccount = CloudStorageAccount.parse(StartScreenActivity.storageConnectionString);

            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.createCloudBlobClient();


            CloudBlobContainer container = blobClient.getContainerReference("blobspeichercontainer");

            CloudBlobDirectory directory = null;
            Iterable<ListBlobItem> blobs = container.listBlobs();

            // only directories here, another for needed to scan files
            for (ListBlobItem blob : blobs) {
                if (blob instanceof CloudBlobDirectory) {
                    directory = (CloudBlobDirectory) blob;
                }
            }
            //Directory: AR-App-hub/01/
            CloudBlobDirectory cloudBlob = null;
            Iterable<ListBlobItem> fileBlobs = directory.listBlobs();
            for (ListBlobItem fileBlob : fileBlobs) {
                if (fileBlob instanceof CloudBlobDirectory) {
                    cloudBlob = (CloudBlobDirectory) fileBlob;
                }
            }
            //Directory: AR-App-hub/01/2019/
            CloudBlobDirectory cloudBlobYear = null;
            Iterable<ListBlobItem> fileBlobs1 = cloudBlob.listBlobs();
            for (ListBlobItem fileBlob1 : fileBlobs1) {
                if (fileBlob1 instanceof CloudBlobDirectory) {
                    cloudBlobYear = (CloudBlobDirectory) fileBlob1;
                }
            }
            //Directory: AR-App-hub/01/2019/01/
            CloudBlobDirectory cloudBlobMonth = null;
            Iterable<ListBlobItem> fileBlobsMonth = cloudBlobYear.listBlobs();
            for (ListBlobItem fileBlob1 : fileBlobsMonth) {
                if (fileBlob1 instanceof CloudBlobDirectory) {
                    cloudBlobMonth = (CloudBlobDirectory) fileBlob1;
                }
            }
            //Directory: AR-App-hub/01/2019/01/23/
            CloudBlobDirectory cloudBlobDay = null;
            Iterable<ListBlobItem> fileBlobsDay = cloudBlobMonth.listBlobs();
            for (ListBlobItem fileBlob : fileBlobsDay) {
                if (fileBlob instanceof CloudBlobDirectory) {
                    cloudBlobDay = (CloudBlobDirectory) fileBlob;
                }
            }
            //Directory: AR-App-hub/01/2019/01/23/18/
            CloudBlobDirectory cloudBlobHour = null;
            Iterable<ListBlobItem> fileBlobsHour = cloudBlobDay.listBlobs();
            for (ListBlobItem fileBlob : fileBlobsHour) {
                if (fileBlob instanceof CloudBlobDirectory) {
                    cloudBlobHour = (CloudBlobDirectory) fileBlob;
                }
            }
            //Directory: AR-App-hub/01/2019/01/23/18/Minute
            CloudBlockBlob lastCloudBlob = null;
            try {
                Iterable<ListBlobItem> blobss = cloudBlobHour.listBlobs();
                for (ListBlobItem fileBlob : blobss) {
                    if (fileBlob instanceof CloudBlockBlob) {
                        lastCloudBlob = (CloudBlockBlob) fileBlob;
                    }
                }
            } catch (StorageException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            System.out.println("LastBlob: " + lastCloudBlob.getUri());
            Log.d("Blob", "Latest Blob: "+lastCloudBlob.getUri());
            String liveDataString = lastCloudBlob.downloadText();
            int indexR1 = liveDataString.lastIndexOf("\"R1 \",");
            String info = liveDataString.substring(indexR1);
            int indexR1_2 = info.indexOf("}");
            String r1Data = info.substring(0, indexR1_2);

            String device = r1Data.substring(1, 4);
            deviceID = device;

            int separator = r1Data.indexOf(": ");
            String restStr = r1Data.substring(separator + 2);
            separator = restStr.indexOf(",");
            noise = restStr.substring(0, separator);

            separator = restStr.indexOf(": ");
            restStr = restStr.substring(separator + 2);
            separator = restStr.indexOf(",");
            voc = restStr.substring(0, separator);

            separator = restStr.indexOf(": ");
            if (restStr.substring(separator + 2) == "True") {
                occurency = "detected";
            } else {
                occurency = "not detected";
            }

            if (deviceID.contains("R1")) {
                room = "O107";
            } else {
                room = "no room";
            }
            result[0] = deviceID;
            result[1] = noise;
            result[2] = voc;
            result[3] = occurency;
            result[4] = room;


        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (Exception e) {
            // Output the stack trace.
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void onPostExecute(String[] strings) {
        //super.onPostExecute(strings);
        live.processFinish(strings);
    }
}

