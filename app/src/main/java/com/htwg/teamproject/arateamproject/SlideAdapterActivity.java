package com.htwg.teamproject.arateamproject;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SlideAdapterActivity extends PagerAdapter {
    Context context;
    LayoutInflater inflater;
    SharedPreferences shared;
    String device;
    String noise;
    String voc;
    String occurency;
    String room;


    public SlideAdapterActivity(Context context) {
        this.context = context;
        handleData();
    }

    public void handleData() {
        shared = context.getSharedPreferences("data", 0);
        device = shared.getString("device", "No device there.");
        noise = shared.getString("noise", "No noise detected.");
        voc = shared.getString("voc", "No voc detected.");
        occurency = shared.getString("occurency", "nothing happen.");
        room = shared.getString("room", "No room.");
    }

    public int[] imagesArray = {R.drawable.ganzes, R.drawable.ganzes};
    public String[] titleArray = {"O-205", "O-206"};
    public String[] descriptionArray = {"Das ist der erste Raum", "Das ist der zweite Raum"};


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

    @Override
    public int getCount() {
        return titleArray.length;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.room_layout_without_popoup, container, false);


        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.room_one_without_popup);
        ImageView imageView = (ImageView) view.findViewById(R.id.slideimg);
        TextView textViewRoomName = (TextView) view.findViewById(R.id.roomName);

        /* ROOM WITHOUT POPUP */
        imageView.setImageResource(imagesArray[position]);
        textViewRoomName.setText(titleArray[position]);

        TextView textViewDeviceData = (TextView) view.findViewById(R.id.deviceData);
        textViewDeviceData.setText(device);

        TextView textViewNoiceData = (TextView) view.findViewById(R.id.noiseData);
        textViewNoiceData.setText(noise + "%");

        TextView textViewVocData = (TextView) view.findViewById(R.id.vocData);
        textViewVocData.setText(voc + "%");

        container.addView(view);
        return view;

    }


}
