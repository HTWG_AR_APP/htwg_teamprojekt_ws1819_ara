package com.htwg.teamproject.arateamproject.pathFinding;

import com.htwg.teamproject.arateamproject.graphEssentials.Edge;
import com.htwg.teamproject.arateamproject.graphEssentials.Node;

import java.util.ArrayList;
import java.util.List;

public class PathControl {


    public Path findShortestBetween(final Node startNode, final Node targetNode) throws NoPathFoundExeption {
        PathUtil pathUtil = new PathUtil();
        List<Path> possiblePaths = new ArrayList<>();
        boolean pathWasFound = false;

        List<Path> pathList = new ArrayList<>();
        //add initial path in order to not mess up #getLongestPaths
        Path initialPath = new Path();
        pathList.add(0, initialPath);

        initialPath = calculateConnectingPath(pathList, initialPath, startNode, targetNode);
        if (initialPath != null) {
            return initialPath;
        }

        Path possiblePath;
        int additionalNodes = PathFindingConstants.ADDITIONAL_NODES;
        while (0 <= additionalNodes) {
            List<Path> parentPaths = pathUtil.getLongestPaths(pathList);

            for (Path path : parentPaths) {
                possiblePath = calculateConnectingPath(pathList, path, pathUtil.getLatestNode(path), targetNode);
                if (possiblePath != null) {
                    possiblePaths.add(possiblePath);
                    pathWasFound = true;
                    break;
                }
            }
            if (pathWasFound) {
                additionalNodes--;
            }
        }

        return pathUtil.findLightestPath(possiblePaths);
    }


    private Path calculateConnectingPath(final List<Path> pathList, final Path parentPath, final Node startNode, final Node targetNode) {
        List<Edge> edges = startNode.getConnections();

        for (Edge edge : edges) {
            Path currentPath = new PathUtil().createPathOffspring(parentPath, edge);
            pathList.add(currentPath);
            if (targetNodeWasFound(targetNode, edge)) {
                return currentPath;
            }
        }

        //intentional null
        return null;

    }


    private boolean targetNodeWasFound(Node targetNode, Edge edge) {
        return edge.getEndNode().getName().equals(targetNode.getName());
    }


}
