package com.htwg.teamproject.arateamproject;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class IntroPagerAdapter extends PagerAdapter {

    private int [] layouts;
    private LayoutInflater layoutInflater;
    private Context context;


    public IntroPagerAdapter(int[] layouts, Context context){

        this.layouts = layouts;
        this.context = context;
        layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return layouts.length;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object obj) {
        View view = (View)obj;
        container.removeView(view);
    }

    @Override
    public boolean isViewFromObject( View view, Object object) {
        return view==object;
    }

    @Override
    public Object instantiateItem( ViewGroup container, int position) {

        View view = layoutInflater.inflate(layouts[position], container, false);
        container.addView(view);
        return view;
    }

}
