package com.htwg.teamproject.arateamproject;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class FragmentAboutTheApp extends Fragment {

    View view;

    public FragmentAboutTheApp(){

    }
    // Diese App ist das Result eines Projektes der HTWG Konstanz. Die App besitzt einen Augmented Reality Teil und einen nicht Augmented Reality Teil.Der nicht Augmented Reality Teil zeigt die Livedaten von Raspberry PIs aus den Räumen O205 und O206. Diese Livedaten geben an ob sich Bewegung in dem Raum befindet und wie die Luftqualität des Raumes ist. Der Augmented Reality Teil wird mit der Smartphone Kamera realisiert, indem der Anwender die Schilder am Raum Scannt und die Möglichkeit besteht den Stundenplan des Raumes angezeigt zu bekommen oder ein anderer Raum mitangegeben werden kann und man den kürzesten Weg zu diesem Raum angezeigt bekommt. In diesem Teil der App werden ebenfalls die Livedaten der Raspberry Pis angezeigt.
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.about_the_app_fragment, container, false);
        return view;
    }
}
