package com.htwg.teamproject.arateamproject.helper.timeTableHelper;

import com.htwg.teamproject.arateamproject.helper.TimeTableProvider;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class CustomTimeTableProvider implements TimeTableProvider, Serializable {

    protected HashMap timeTableMap = new HashMap();

    @Override
    public boolean roomIsOccupied(String roomKey) throws TimetableForRoomNotFoundException {
        roomExists(roomKey);
        boolean[][] timetable = (boolean[][]) timeTableMap.get(roomKey);
        return timetable[getCurrentDay()][getCurrentQuarter()];
    }


    public ArrayList<String> getFreeRooms() {

        ArrayList freeRooms = new ArrayList();

        Set<Map.Entry<String, boolean[][]>> entrySet = timeTableMap.entrySet();
        for (Map.Entry<String, boolean[][]> entry : entrySet) {
            String currentKey = entry.getKey();
            try {
                if (roomIsFree(currentKey)) {
                    freeRooms.add(currentKey);
                }
            } catch (TimetableForRoomNotFoundException e) {
                e.printStackTrace();
            }
        }

        return freeRooms;
    }


    public boolean roomIsFree(String roomKey) throws TimetableForRoomNotFoundException {
        return !roomIsOccupied(roomKey);
    }

    private boolean roomExists(String roomKey) throws TimetableForRoomNotFoundException {
        if (null == timeTableMap.get(roomKey)) {
            throw new TimetableForRoomNotFoundException();
        }
        return true;
    }

    private int getCurrentQuarter() {
        String timeStampHours = new SimpleDateFormat("HH").format(Calendar.getInstance().getTime());
        String timeStampMinutes = new SimpleDateFormat("mm").format(Calendar.getInstance().getTime());
        int hourCount = (Integer.parseInt(timeStampHours) - 8) * 4;
        int minuteCount = Integer.parseInt(timeStampMinutes) / 15;

        int currentQuater = hourCount + minuteCount;
        if (currentQuater > 49 || currentQuater < 0) {
            return 0;
        }
        return currentQuater;
    }


    private int getCurrentDay() {
        String day = new SimpleDateFormat("EE", Locale.ENGLISH).format(Calendar.getInstance().getTime()).toLowerCase();

        if (day.contains("mo")) {
            return 0;
        } else if (day.contains("tu")) {
            return 1;
        } else if (day.contains("we")) {
            return 2;
        } else if (day.contains("th")) {
            return 3;
        } else if (day.contains("fr")) {
            return 4;
        } else {
            return 0;
        }
    }


}
