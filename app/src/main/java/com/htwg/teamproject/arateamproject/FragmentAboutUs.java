package com.htwg.teamproject.arateamproject;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class FragmentAboutUs extends Fragment {

    View view;

    // Die App wurde von 4 Studierenden der HTWG Konstanz entwickelt. Die Studierenden sind Raphael Sterk, Rindrit Bislimi, Philipp Hug und Caroline Hess. Das Projekt wurde anhand eines Moduls von Frau Sonja Meyer angeboten und in den letzten 6 Monaten betreut. Alle Studierende sind aus dem Studiengang Wirtschaftsinformatik und der Vertiefungsrichtung Software und Systementwicklung. Frau Meyer ist bereits mehrere Jahre in der Softwareentwicklung tätig und nun Professorin an der HTWG.
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.abouts_fragment, container, false);
        return view;
    }

    public FragmentAboutUs() {

    }
}
