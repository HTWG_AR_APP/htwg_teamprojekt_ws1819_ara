package com.htwg.teamproject.arateamproject.graphEssentials;

import com.htwg.teamproject.arateamproject.directionControl.DirectionRelations;

import java.util.HashMap;

public class Graph {


    private HashMap<String, Node> graph = new HashMap<>();


    public void generateAll(DirectionRelations directionRelations) {
        generateNodesFor(graph);
        generateEdgesForGraph();
        generateRelationsForNodes(directionRelations);
    }


    private void generateNodesFor(final HashMap<String, Node> graph) {

        for (int i = 1; i <= 8; i++) {
            String name = "00".concat(String.valueOf(i));
            graph.put(name, new Node(name));
        }

        for (int i = 1; i <= 8; i++) {
            String name = "10".concat(String.valueOf(i));
            graph.put(name, new Node(name));
        }
    }

    private void generateEdgesForGraph() {
        //-----------------------------EG---------------------------------
        generateEdgeFor(getNodeByName("001"), getNodeByName("002"), 14.0);
        generateEdgeFor(getNodeByName("002"), getNodeByName("003"), 15.0);
        generateEdgeFor(getNodeByName("003"), getNodeByName("004"), 3.0);
        generateEdgeFor(getNodeByName("004"), getNodeByName("005"), 8.0);
        generateEdgeFor(getNodeByName("005"), getNodeByName("007"), 9.0);
        generateEdgeFor(getNodeByName("007"), getNodeByName("008"), 21.0);
        generateEdgeFor(getNodeByName("001"), getNodeByName("008"), 13.0);
        //-----------------------------EG---------------------------------

        generateEdgeFor(getNodeByName("001"), getNodeByName("101"), 34.0);
        generateEdgeFor(getNodeByName("005"), getNodeByName("105"), 34.0);

        generateEdgeFor(getNodeByName("001"), getNodeByName("102"), 40.0);
        generateEdgeFor(getNodeByName("005"), getNodeByName("107"), 40.0);

        //-----------------------------1OG---------------------------------
        generateEdgeFor(getNodeByName("101"), getNodeByName("102"), 10.0);
        generateEdgeFor(getNodeByName("102"), getNodeByName("103"), 6.0);
        generateEdgeFor(getNodeByName("103"), getNodeByName("104"), 3.0);
        generateEdgeFor(getNodeByName("104"), getNodeByName("105"), 8.0);
        generateEdgeFor(getNodeByName("105"), getNodeByName("107"), 15.0);
        generateEdgeFor(getNodeByName("107"), getNodeByName("108"), 9.0);
        generateEdgeFor(getNodeByName("101"), getNodeByName("108"), 13.0);
        //-----------------------------1OG---------------------------------

        /*
        //-----------------------------2OG---------------------------------
        generateEdgeFor(getNodeByName("201"), getNodeByName("207a"), 7.0);
        generateEdgeFor(getNodeByName("207a"), getNodeByName("207b"), 7.5);
        generateEdgeFor(getNodeByName("207b"), getNodeByName("205"), 4.0);
        generateEdgeFor(getNodeByName("205"), getNodeByName("206"), 6.5);
        generateEdgeFor(getNodeByName("206"), getNodeByName("207"), 9.0);
        generateEdgeFor(getNodeByName("207"), getNodeByName("208"), 5.5);
        generateEdgeFor(getNodeByName("208"), getNodeByName("209"), 7.5);
        //-----------------------------2OG---------------------------------
        */
    }


    private void generateRelationsForNodes(DirectionRelations directionRelations) {

        directionRelations.generateRelationBetweenMultiDirection(getNodeByName("001"), getNodeByName("002"), "right", "left");
        directionRelations.generateRelationBetweenMultiDirection(getNodeByName("002"), getNodeByName("003"), "right", "left");
        directionRelations.generateRelationBetweenMultiDirection(getNodeByName("003"), getNodeByName("004"), "right", "left");
        directionRelations.generateRelationBetweenMultiDirection(getNodeByName("004"), getNodeByName("005"), "right", "left");
        directionRelations.generateRelationBetweenMultiDirection(getNodeByName("005"), getNodeByName("007"), "right", "left");
        directionRelations.generateRelationBetweenMultiDirection(getNodeByName("007"), getNodeByName("008"), "right", "left");
        directionRelations.generateRelationBetweenMultiDirection(getNodeByName("008"), getNodeByName("001"), "back", "back");

        directionRelations.generateRelationBetweenMultiDirection(getNodeByName("001"), getNodeByName("101"), "up", "down");
        directionRelations.generateRelationBetweenMultiDirection(getNodeByName("005"), getNodeByName("105"), "up", "down");


        directionRelations.generateRelationBetweenMultiDirection(getNodeByName("001"), getNodeByName("102"), "up", "down");
        directionRelations.generateRelationBetweenMultiDirection(getNodeByName("005"), getNodeByName("107"), "up", "down");



        directionRelations.generateRelationBetweenMultiDirection(getNodeByName("101"), getNodeByName("102"), "right", "left");
        directionRelations.generateRelationBetweenMultiDirection(getNodeByName("102"), getNodeByName("103"), "right", "left");
        directionRelations.generateRelationBetweenMultiDirection(getNodeByName("103"), getNodeByName("104"), "right", "left");
        directionRelations.generateRelationBetweenMultiDirection(getNodeByName("104"), getNodeByName("105"), "right", "left");
        directionRelations.generateRelationBetweenMultiDirection(getNodeByName("105"), getNodeByName("107"), "right", "left");
        directionRelations.generateRelationBetweenMultiDirection(getNodeByName("107"), getNodeByName("108"), "right", "left");
        directionRelations.generateRelationBetweenMultiDirection(getNodeByName("108"), getNodeByName("101"), "back", "back");
    }


    private void generateEdgeFor(final Node startNode, final Node endNode, final Double weight) {
        startNode.addConnection(new Edge(startNode, endNode, weight));
        endNode.addConnection(new Edge(endNode, startNode, weight));
    }


    public Node getNodeByName(final String name) {
        return graph.get(name);
    }


}
