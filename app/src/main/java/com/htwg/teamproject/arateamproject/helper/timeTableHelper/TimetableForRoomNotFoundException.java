package com.htwg.teamproject.arateamproject.helper.timeTableHelper;

public class TimetableForRoomNotFoundException extends Exception {


    public TimetableForRoomNotFoundException() {
        super("There is no timetable for this room!");
    }

    public TimetableForRoomNotFoundException(String message) {
        super(message);
    }


}
