package com.htwg.teamproject.arateamproject.directionControl;

import com.htwg.teamproject.arateamproject.graphEssentials.Node;


public class Relation {

    private Node startNode;
    private Node endNode;

    private Direction direction;

    /**@param startNode starting from here
     * @param endNode ending here
     * @param direction if startNode is left and endNode is right, direction needs to be right.
     * */
    public Relation(Node startNode, Node endNode, Direction direction) {
        this.startNode = startNode;
        this.endNode = endNode;
        this.direction = direction;
    }

    public Node getStartNode() {
        return startNode;
    }

    public void setStartNode(Node startNode) {
        this.startNode = startNode;
    }

    public Node getEndNode() {
        return endNode;
    }

    public void setEndNode(Node endNode) {
        this.endNode = endNode;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }
}
