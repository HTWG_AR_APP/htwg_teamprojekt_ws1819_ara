package com.htwg.teamproject.arateamproject;

public interface LiveData {
    void processFinish(String[] output);
}
