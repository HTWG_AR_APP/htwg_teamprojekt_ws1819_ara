package com.htwg.teamproject.arateamproject.helper.timeTableHelper;

import android.os.Parcel;
import android.os.Parcelable;

import com.htwg.teamproject.arateamproject.helper.TimeTableProvider;

public class TimeTableProviderContainer implements Parcelable {

    private TimeTableProvider ttp;

    public TimeTableProviderContainer(TimeTableProvider ttp) {
        this.ttp = ttp;
    }

    protected TimeTableProviderContainer(Parcel in) {
    }

    public static final Creator<TimeTableProviderContainer> CREATOR = new Creator<TimeTableProviderContainer>() {
        @Override
        public TimeTableProviderContainer createFromParcel(Parcel in) {
            return new TimeTableProviderContainer(in);
        }

        @Override
        public TimeTableProviderContainer[] newArray(int size) {
            return new TimeTableProviderContainer[size];
        }
    };

    public TimeTableProvider getTtp() {
        return ttp;
    }

    public void setTtp(TimeTableProvider ttp) {
        this.ttp = ttp;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }
}
