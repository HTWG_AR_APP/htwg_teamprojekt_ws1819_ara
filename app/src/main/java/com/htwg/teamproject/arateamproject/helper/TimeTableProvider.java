package com.htwg.teamproject.arateamproject.helper;

import com.htwg.teamproject.arateamproject.helper.timeTableHelper.TimetableForRoomNotFoundException;

public interface TimeTableProvider {

    public boolean roomIsOccupied(String roomKey) throws TimetableForRoomNotFoundException;


}
