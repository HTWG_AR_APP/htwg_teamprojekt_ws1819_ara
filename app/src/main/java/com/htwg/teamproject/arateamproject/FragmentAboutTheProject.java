package com.htwg.teamproject.arateamproject;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class FragmentAboutTheProject extends Fragment {

    View view;

    public FragmentAboutTheProject() {

    }

    //Dieses Projekt ist entwickelt worden von Studierenden aus dem Studiengang Wirtschaftsinformatik von der Hochschule für Technik, Wirtschaft und Gestaltung in Konstanz. In dem Bachelorstudiengang Wirtschaftsinformatik gibt es im 6. Semester das Modul Teamprojekt, wo die Studierenden in Teams entscheiden für welches Projekt Sie sich interessieren. Die unterschiedlichsten Projekte werden von den Professorinnnen und Professoren online beschrieben und den Studierenden zur Verfügung gestellt. Bei diesem Projekt handelt es sich um die Entwicklung einer Augmented Reality App im Bereich des Gebäudemanagement, dass von Frau Sonja Meyer angeboten wurde.
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.about_the_project_fragment, container, false);

        return view;
    }
}
