package com.htwg.teamproject.arateamproject.pathFinding;

import com.htwg.teamproject.arateamproject.graphEssentials.Edge;
import com.htwg.teamproject.arateamproject.graphEssentials.Node;

import java.util.ArrayList;
import java.util.List;

public class PathUtil {

    Path findLightestPath(List<Path> pathList) {

        Path LightestPath = pathList.get(0);
        double leastWeight = pathList.get(0).getWeight();

        for (Path path : pathList) {
            double currentWeight = path.getWeight();
            if (currentWeight <= leastWeight) {
                leastWeight = currentWeight;
                LightestPath = path;
            }
        }
        return LightestPath;
    }


    Path createPathOffspring(Path parentPath, Edge edge) {

        Path currentPath = new Path();
        currentPath.setEdges((ArrayList<Edge>) parentPath.getEdges().clone());
        currentPath.addEdge(edge);
        currentPath.setWeight(parentPath.getWeight());
        currentPath.addWeight(edge.getWeight());
        currentPath.setLength(parentPath.getLength());
        currentPath.addLength();
        return currentPath;

    }


    List<Path> getLongestPaths(List<Path> pathList) {

        ArrayList<Path> longestPaths = new ArrayList<>();

        int lastListIndex = pathList.size() - 1;
        int latestPathLength = pathList.get(lastListIndex).getLength();

        int currentPathLength = latestPathLength;
        int loopIndex = lastListIndex;

        while (currentPathLength == latestPathLength) {
            longestPaths.add(pathList.get(loopIndex));
            loopIndex--;
            currentPathLength = pathList.get(loopIndex).getLength();
        }
        return longestPaths;
    }


    Node getLatestNode(Path path) {
        List<Edge> edges = path.getEdges();
        return edges.get(edges.size() - 1).getEndNode();
    }

}
