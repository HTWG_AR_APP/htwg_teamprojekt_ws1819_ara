package com.htwg.teamproject.arateamproject;

import android.util.Log;

import com.htwg.teamproject.arateamproject.directionControl.Direction;
import com.htwg.teamproject.arateamproject.directionControl.DirectionRelations;
import com.htwg.teamproject.arateamproject.directionControl.NoNeighbourNodeFoundExeption;
import com.htwg.teamproject.arateamproject.graphEssentials.Edge;
import com.htwg.teamproject.arateamproject.graphEssentials.Graph;
import com.htwg.teamproject.arateamproject.graphEssentials.Node;
import com.htwg.teamproject.arateamproject.pathFinding.NoPathFoundExeption;
import com.htwg.teamproject.arateamproject.pathFinding.Path;
import com.htwg.teamproject.arateamproject.pathFinding.PathControl;
import com.htwg.teamproject.arateamproject.pathFinding.PathUtil;

import java.util.ArrayList;
import java.util.List;

public class DirectionProvider {


    /**
     * @param startNode  Key for starting Node (e.g. 001)
     * @param targetNode Key for target Node (e.g. 205)
     * @return Direction element. -> has methods like boolean.isRight...
     * @throws NoNeighbourNodeFoundExeption if there is no neighbour
     */
    public Direction provideDirectionFor(final String startNode, final String targetNode) throws NoNeighbourNodeFoundExeption, NoPathFoundExeption {

        Graph graph = new Graph();
        DirectionRelations directionRelations = new DirectionRelations();
        graph.generateAll(directionRelations);

        if (startNode.equals(targetNode)) {
            return new Direction("here");
        }


        Path path = providePathFor(startNode, targetNode);
        Edge affectedEdge = path.getEdges().get(0);

        return directionRelations.getRelationfor(affectedEdge.getStartNode(), affectedEdge.getEndNode()).getDirection();
    }

    private Path providePathFor(final String startNode, final String targetNode) throws NoNeighbourNodeFoundExeption, NoPathFoundExeption {
        Graph graph = new Graph();
        DirectionRelations directionRelations = new DirectionRelations();
        graph.generateAll(directionRelations);


        if (!inputStringIsValid(startNode, graph) || !inputStringIsValid(targetNode, graph)) {
            throw new NoNeighbourNodeFoundExeption("Non-valid input-string was entered. Therefore no neighbour can be found.");
        }
        return new PathControl().findShortestBetween(graph.getNodeByName(startNode), graph.getNodeByName(targetNode));
    }


    public String provideClosestFreeRoom(final String startNode, final ArrayList<String> roomKeys) throws NoNeighbourNodeFoundExeption, NoPathFoundExeption {

        if (roomKeys.size() == 0) {
            throw new NoPathFoundExeption("No paths could be calculated, since there are no free Rooms");
        }

        double weight = -1;
        String closetRoomKey = "";

        for (String roomKey : roomKeys) {

            try {
                Path path = providePathFor(startNode, roomKey);
                double currentWeight = path.getWeight();
                if (weight == -1 || currentWeight < weight) {
                    weight = currentWeight;
                    closetRoomKey = roomKey;
                }

            } catch (NoNeighbourNodeFoundExeption noNeighbourNodeFoundExeption) {
                Log.d("No Path Found", noNeighbourNodeFoundExeption.getMessage());
            }

        }

        if (weight == -1) {
            throw new NoPathFoundExeption("No paths could be calculated. There may not be a free Room");
        }
        return closetRoomKey;
    }


    private boolean inputStringIsValid(String nodeID, Graph graph) {
        return graph.getNodeByName(nodeID) != null;
    }



        public double provideWeightBetweenRooms(String roomKey, String targetKey) throws NoNeighbourNodeFoundExeption, NoPathFoundExeption {
            Path path = providePathFor(roomKey, targetKey);
            return path.getEdges().get(0).getWeight();


        }


}
