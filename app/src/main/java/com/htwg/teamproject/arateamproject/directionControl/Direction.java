package com.htwg.teamproject.arateamproject.directionControl;

public class Direction {

    private boolean here, up, down, left, right, front, back, upFront, downFront;

    public Direction(String direction) {
        if (direction.equals("here")) here = true;
        if (direction.equals("up")) up = true;
        if (direction.equals("down")) down = true;
        if (direction.equals("left")) left = true;
        if (direction.equals("right")) right = true;
        if (direction.equals("front")) front = true;
        if (direction.equals("back")) back = true;
        if (direction.equals("upFront")) upFront = true;
        if (direction.equals("downFront")) downFront = true;
    }

    public boolean isHere() {
        return here;
    }

    public void setHere(boolean here) {
        this.here = here;
    }

    public boolean isUp() {
        return up;
    }

    public void setUp(boolean up) {
        this.up = up;
    }

    public boolean isDown() {
        return down;
    }

    public void setDown(boolean down) {
        this.down = down;
    }

    public boolean isLeft() {
        return left;
    }

    public void setLeft(boolean left) {
        this.left = left;
    }

    public boolean isRight() {
        return right;
    }

    public void setRight(boolean right) {
        this.right = right;
    }

    public boolean isFront() {
        return front;
    }

    public void setFront(boolean front) {
        this.front = front;
    }

    public boolean isBack() {
        return back;
    }

    public void setBack(boolean back) {
        this.back = back;
    }

    public boolean isUpFront() {
        return upFront;
    }

    public void setUpFront(boolean upfront) {
        this.upFront = upfront;
    }

    public boolean isdownFront() {
        return downFront;
    }

    public void setdownFront(boolean downFront) {
        this.downFront = downFront;
    }

    public void resetAll() {
        here = false;
        up = false;
        down = false;
        left = false;
        right = false;
        front = false;
        back = false;
        upFront = false;
    }
}
