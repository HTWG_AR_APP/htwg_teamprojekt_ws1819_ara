package com.htwg.teamproject.arateamproject.helper;

import com.htwg.teamproject.arateamproject.helper.timeTableHelper.TimeTable;
import com.htwg.teamproject.arateamproject.helper.timeTableHelper.CustomTimeTableProvider;

public class FetchTimeTablesRunnable implements Runnable {

    private TimeTableProviderContainer container;

    public FetchTimeTablesRunnable(TimeTableProviderContainer container) {
        this.container = container;
    }

    @Override
    public void run() {
        CustomTimeTableProvider ttp = new CustomTimeTableProvider();
        new TimeTable(ttp);
        container.setTimeTableProvider(ttp);
    }

}
