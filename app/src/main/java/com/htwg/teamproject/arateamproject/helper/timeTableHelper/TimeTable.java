package com.htwg.teamproject.arateamproject.helper.timeTableHelper;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @deprecated this really should be coded in a nicer way lol. This workaround shall only be used, until a proper api is provided.
 */
public class TimeTable {

    CustomTimeTableProvider ttc;

    public TimeTable(CustomTimeTableProvider ttc) {
        this.ttc = ttc;
        fetch();
        Log.d("activity", "Done fetching timetables");
    }


    public void fetch() {
        String url = "https://lsf.htwg-konstanz.de/qisserver/rds?state=wplan&act=Raum&pool=Raum&show=plan&P.subc=plan";
        ArrayList<String> linkArrayList = new ArrayList<>();
        ArrayList<String> roomKeyArrayList = new ArrayList<>();

        try {
            URL obj = new URL(url);
            HttpURLConnection urlConnection = (HttpURLConnection) obj.openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

            String inputLine;
            Pattern pattern = Pattern.compile("https://lsf.htwg-konstanz.de/qisserve.*text=O.*\"");
            Pattern innerPattern = Pattern.compile("rgid=[0-9]*");
            Pattern keyPattern = Pattern.compile("=n&amp;text=O\\+-\\+[0-9]{3}\\+\\+");

            while ((inputLine = br.readLine()) != null) {
                Matcher matcher = pattern.matcher(inputLine);
                Matcher keyMatcher = keyPattern.matcher(inputLine);
                if (keyMatcher.find()) {
                    roomKeyArrayList.add(keyMatcher.group().substring(16, 19));
                }
                if (matcher.find()) {
                    String matchedLine = matcher.group();
                    Log.d("URL", matchedLine);
                    Matcher innerMatcher = innerPattern.matcher(matchedLine);
                    if (innerMatcher.find()) {
                        linkArrayList.add(innerMatcher.group());
                    }
                }
            }
        } catch (Exception e) {
            Log.d("Fetchingerror", e.getMessage());
        }
        fetchAllTimeTables(linkArrayList, roomKeyArrayList);
    }

    private void fetchAllTimeTables(ArrayList<String> linkArrayList, ArrayList<String> roomKeyArrayList) {
        linkArrayList.size();
        for (int i = 0; i <= linkArrayList.size() - 1; i++) {
            ttc.timeTableMap.put(roomKeyArrayList.get(i), getTimetableFor(linkArrayList.get(i)));
            try {
                new Thread().sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    private boolean[][] getTimetableFor(String rgidId) {

        boolean processingStarted = false;
        int row = 0;
        int day = 0;
        boolean[][] timetable = new boolean[5][50];

        Pattern pattern = Pattern.compile("rowspan=\"[1-9]?[0-9]\"");

        try {
            URL obj2 = new URL("https://lsf.htwg-konstanz.de/qisserver/rds?state=wplan&act=Raum&pool=Raum&P.subc=plan&raum.".concat(rgidId));
            HttpURLConnection urlConnection = (HttpURLConnection) obj2.openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String inputLine;
            while ((inputLine = br.readLine()) != null) {
                Matcher matcher = pattern.matcher(inputLine);
                if (processingStarted && newRowStarted(inputLine)) {
                    row++;
                }
                if (!processingStarted && inputLine.contains("<tr style=\"background-color:#DFDFDF\">")) {
                    processingStarted = true;
                    row++;
                }
                if (processingStarted && inputLine.contains("<td width=\"0\">")) {
                    day = -1;
                }
                if (processingStarted && (inputLine.contains("plan1") || inputLine.contains("plan2"))) {
                    day++;
                    day = day % 5;
                    day = calculateActualDay(day, row, timetable);
                    int amount = extractNumber(matcher);
                    addAmountToArray(timetable, day, row, amount);
                }
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            e.getMessage();
        }

        return timetable;
    }

    private int extractNumber(Matcher matcher) {

        if (matcher.find()) {
            String matchedLine = matcher.group();
            char secondLastIndex = matchedLine.charAt(matchedLine.length() - 2);
            char thirdLastIndex = matchedLine.charAt(matchedLine.length() - 3);

            if (thirdLastIndex >= '0' && thirdLastIndex <= '9') {
                return Integer.parseInt(new StringBuilder().append(thirdLastIndex).append(secondLastIndex).toString());
            }
            return Integer.parseInt(new StringBuilder().append(secondLastIndex).toString());
        }
        return 0;
    }


    private int calculateActualDay(int day, int row, boolean[][] timeTable) {
        if (day >= 4) {
            return 4;
        }
        if (timeTable[day][row]) {
            return calculateActualDay(day + 1, row, timeTable);
        }
        return day;
    }

    private boolean newRowStarted(String inputLine) {
        return inputLine.contains("<tr style=\"background-color:#DFDFDF\">") || inputLine.contains("<tr style=\"background-color:#F1F2F5\">");
    }

    private void addAmountToArray(boolean[][] timeTable, int day, int row, int amount) {
        for (int i = 0; i < amount; i++) {
            timeTable[day][row + i] = true;
        }
    }

}
