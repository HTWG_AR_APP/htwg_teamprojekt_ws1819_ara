package com.htwg.teamproject.arateamproject.helper;

import android.util.Log;

public class TextFilter {


    public String filterTextForRoomNumber(String text) {
        int lastRoomNumberIndex = containsLastNumbersAt(text);
        Log.d("roomnumber", "|" + text + "|" + lastRoomNumberIndex);
        if (lastRoomNumberIndex >= 3) {

            String substring = text.substring(lastRoomNumberIndex - 3, lastRoomNumberIndex);

            if (consitsOfNumbers(substring)) {
                return substring;
            }
        }
        return "No number found!";
    }


    /**
     * @param text
     * @return index of last number in a String
     */
    private int containsLastNumbersAt(String text) {
        String numberString = "0123456789";
        int inexOfLastFoundNumber = -1;
        for (int i = 0; i < text.length(); i++) {
            if (numberString.contains(Character.toString(text.charAt(i)))) {
                inexOfLastFoundNumber = i + 1;
            }
        }
        return inexOfLastFoundNumber;
    }

    private boolean consitsOfNumbers(String text) {
        String numberString = "0123456789";
        for (int i = 0; i < text.length(); i++) {
            if (!numberString.contains(Character.toString(text.charAt(i)))) {
                return false;
            }
        }
        return true;
    }

}
