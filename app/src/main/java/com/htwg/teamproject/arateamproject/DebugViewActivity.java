package com.htwg.teamproject.arateamproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.htwg.teamproject.arateamproject.directionControl.Direction;
import com.htwg.teamproject.arateamproject.directionControl.NoNeighbourNodeFoundExeption;
import com.htwg.teamproject.arateamproject.pathFinding.NoPathFoundExeption;
import com.htwg.teamproject.arateamproject.pathFinding.PathFindingConstants;

import java.util.ArrayList;

public class DebugViewActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener{

    //-----------------Debuggingkram--------------------------
    String startNode, targetNode, freeAndSearchRoomKey;
    ArrayList<String> freeRooms = new ArrayList<>();


    EditText startNodeInput;
    EditText targetNodeInput;
    EditText freshAirNode;
    EditText freeRoomNode;

    Button freshAirButton;
    Button submitButton;
    Button enterButton;
    Button searchFreeRoom;

    Switch janitorSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debug_view);


        //-----------------Debuggingkram--------------------------
        setUpRoomDebugging();
        //-----------------Debuggingkram--------------------------

        janitorSwitch = (Switch) findViewById(R.id.switch3);
        janitorSwitch.setOnCheckedChangeListener(this);
        if(PathFindingConstants.janitorMode){
            janitorSwitch.toggle();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            PathFindingConstants.janitorMode = true;
        } else {
            PathFindingConstants.janitorMode = false;
        }
    }




    //-----------------Debuggingkram--------------------------
    private void setUpRoomDebugging() {
        startNodeInput = (EditText) findViewById(R.id.startNode);
        targetNodeInput = (EditText) findViewById(R.id.targetNode);
        freshAirNode = (EditText) findViewById(R.id.freshAirNode);
        freeRoomNode = (EditText) findViewById(R.id.freeRoomNode);

        submitButton = (Button) findViewById(R.id.search);
        freshAirButton = (Button) findViewById(R.id.freshAirAlarm);
        enterButton = (Button) findViewById(R.id.enterFreeRoom);
        searchFreeRoom = (Button) findViewById(R.id.searchFreeRoom);

        freshAirButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PathFindingConstants.freshAirRoom = freshAirNode.getText().toString();
                showToast("In dem Raum\"" + PathFindingConstants.freshAirRoom + "\", muss das Fenster geöffnet werden.");
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startNode = startNodeInput.getText().toString();
                targetNode = targetNodeInput.getText().toString();

                processSearch(startNode, targetNode);
            }
        });

        enterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                freeAndSearchRoomKey = freeRoomNode.getText().toString();
                freeRooms.add(freeAndSearchRoomKey);
            }
        });

        searchFreeRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                freeAndSearchRoomKey = freeRoomNode.getText().toString();
                searchClosestFreeRoom(freeAndSearchRoomKey, freeRooms);
            }
        });

    }


    private void searchClosestFreeRoom(String startNodeId, ArrayList<String> freeRooms) {
        try {
            showToast(new DirectionProvider().provideClosestFreeRoom(startNodeId, freeRooms));
        } catch (NoNeighbourNodeFoundExeption noNeighbourNodeFoundExeption) {
            noNeighbourNodeFoundExeption.printStackTrace();
        } catch (NoPathFoundExeption noPathFoundExeption) {
            noPathFoundExeption.printStackTrace();
        }
    }

    private void processSearch(String startNodeId, String targetNodeId) {
        try {
            Direction direction = new DirectionProvider().provideDirectionFor(startNodeId, targetNodeId);
            if (direction.isLeft()) showToast("Go Left.");
            if (direction.isRight()) showToast("Go Right.");
            if (direction.isBack()) showToast("Turn around.");
            if (direction.isFront()) showToast("Go Straight.");
            if (direction.isHere()) showToast("Arrived!");
            if (direction.isUp()) showToast("Go Upstairs");
            if (direction.isDown()) showToast("Go Downstairs");

        } catch (NoNeighbourNodeFoundExeption noNeighbourNodeFoundExeption) {
            showToast(noNeighbourNodeFoundExeption.getMessage());
        } catch (NoPathFoundExeption noPathFoundExeption) {
            showToast(noPathFoundExeption.getMessage());
        }

    }

    private void showToast(String txt) {
        Toast.makeText(DebugViewActivity.this, txt, Toast.LENGTH_LONG).show();
    }
//-----------------Debuggingkram--------------------------
}
