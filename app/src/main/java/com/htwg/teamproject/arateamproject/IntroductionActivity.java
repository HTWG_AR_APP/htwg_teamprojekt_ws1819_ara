package com.htwg.teamproject.arateamproject;

import android.content.Intent;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class IntroductionActivity extends AppCompatActivity implements View.OnClickListener {

    private ViewPager viewPager;
    private int[] layouts = {R.layout.introduction_first_screen, R.layout.introduction_second_screen,
            R.layout.introduction_third_screen};
    private IntroPagerAdapter introPagerAdapter;

    private LinearLayout Dots_Layout;
    private ImageView[] dots;

    private Button bn_next, bn_skip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT > 19) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        setContentView(R.layout.activity_introduction);

        viewPager = (ViewPager) findViewById(R.id.viewIntro);
        introPagerAdapter = new IntroPagerAdapter(layouts, this);
        viewPager.setAdapter(introPagerAdapter);

        Dots_Layout = (LinearLayout) findViewById(R.id.layoutDots);

        bn_next = (Button) findViewById(R.id.btn_next);
        bn_skip = (Button) findViewById(R.id.btn_skip);
        bn_next.setOnClickListener(this);
        bn_skip.setOnClickListener(this);

        createDots(0);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {
                createDots(position);

                if (position == layouts.length - 1) {
                    bn_next.setText("Home");
                    bn_skip.setVisibility(View.INVISIBLE);
                } else {
                    bn_next.setText("Next");
                    bn_skip.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }


    private void createDots(int current_position) {
        if (Dots_Layout != null) {
            Dots_Layout.removeAllViews();

            dots = new ImageView[layouts.length];

            for (int i = 0; i < layouts.length; i++) {

                dots[i] = new ImageView(this);
                if (i == current_position) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.active_dots));
                } else {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.default_dots));
                }

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);

                params.setMargins(4, 0, 4, 0);

                Dots_Layout.addView(dots[i], params);
            }
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btn_next:
                loadNextSlide();
                break;

            case R.id.btn_skip:
                skipToHome();
                break;
        }

    }

    private void skipToHome() {
        Intent intent = new Intent(this, HomeScreenActivity.class);
        intent.putExtra("ttpr", getIntent().getSerializableExtra("ttpr"));
        startActivity(intent);


        finish();
    }

    private void loadNextSlide() {
        int next_slide = viewPager.getCurrentItem() + 1;

        if (next_slide < layouts.length) {
            viewPager.setCurrentItem(next_slide);
        } else {
            skipToHome();
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(IntroductionActivity.this, HomeScreenActivity.class);
        intent.putExtra("ttpr", getIntent().getSerializableExtra("ttpr"));
        startActivity(intent);
    }

}
