package com.htwg.teamproject.arateamproject.pathFinding;

public class NoPathFoundExeption extends Exception {

    public NoPathFoundExeption(String message) {
        super(message);
    }

}
