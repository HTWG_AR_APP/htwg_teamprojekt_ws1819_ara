package com.htwg.teamproject.arateamproject.pathFinding;

import com.htwg.teamproject.arateamproject.graphEssentials.Edge;

import java.util.ArrayList;

public class Path {

    private double weight;
    private ArrayList<Edge> edges = new ArrayList<>();
    private int length = 0;

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public ArrayList<Edge> getEdges() {
        return edges;
    }

    public int getLength() {
        return length;
    }

    public void setEdges(ArrayList<Edge> edges) {
        this.edges = edges;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public void addWeight(double weight) {
        this.weight += weight;
    }

    public void addEdge(Edge edge) {
        edges.add(edge);
    }

    public void addLength() {
        length++;
    }
}
