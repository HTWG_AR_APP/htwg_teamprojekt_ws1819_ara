package com.htwg.teamproject.arateamproject;

import android.content.Intent;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class AboutUsActivity extends AppCompatActivity {


    private TabLayout tabLayout;
    private AppBarLayout appBarLayout;
    private ViewPager viewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        tabLayout = (TabLayout) findViewById(R.id.tablayout_id);
        appBarLayout = (AppBarLayout) findViewById(R.id.appbarid);
        viewPager = (ViewPager) findViewById(R.id.viewpager_aboutUs);
        AboutUsAdapter aboutUsAdapter = new AboutUsAdapter(getSupportFragmentManager());

        //Hinzufügen von Fragmenten.
        aboutUsAdapter.AddFragment(new FragmentAboutUs(), "About Us");
        aboutUsAdapter.AddFragment(new FragmentAboutTheApp(), "About the App");
        aboutUsAdapter.AddFragment(new FragmentAboutTheProject(), "About the Project");

        //Adapter
        viewPager.setAdapter(aboutUsAdapter);
        tabLayout.setupWithViewPager(viewPager);

    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(AboutUsActivity.this, HomeScreenActivity.class);
        intent.putExtra("ttpr", getIntent().getSerializableExtra("ttpr"));
        startActivity(intent);
    }

}
