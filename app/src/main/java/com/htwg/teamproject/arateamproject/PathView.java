package com.htwg.teamproject.arateamproject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.ar.sceneform.ArSceneView;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.math.Quaternion;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.rendering.ViewRenderable;
import com.google.ar.sceneform.ux.ArFragment;
import com.htwg.teamproject.arateamproject.directionControl.Direction;
import com.htwg.teamproject.arateamproject.directionControl.NoNeighbourNodeFoundExeption;
import com.htwg.teamproject.arateamproject.helper.TimeTableProvider;
import com.htwg.teamproject.arateamproject.helper.TimeTableProviderContainer;
import com.htwg.teamproject.arateamproject.helper.timeTableHelper.CustomTimeTableProvider;
import com.htwg.teamproject.arateamproject.helper.timeTableHelper.TimetableForRoomNotFoundException;
import com.htwg.teamproject.arateamproject.pathFinding.NoPathFoundExeption;
import com.htwg.teamproject.arateamproject.pathFinding.PathFindingConstants;

import java.util.ArrayList;

public class PathView extends AppCompatActivity {


    ArSceneView arSceneView;
    ArFragment arFragment;
    ModelRenderable arrowRenderable;
    ModelRenderable markerRenderable;
    Node arrowHere = new Node();
    Node markerDestination = new Node();
    Node info = new Node();

    TimeTableProviderContainer ttpr;
    Button showMore;

    boolean isFree = true;

    TextView closestRoom;

    SharedPreferences shared;
    String device;
    String noise;
    String voc;
    String occurency;
    String room;
    Bundle b;
    String freeRoomNr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_path_view);


        arFragment = (ArFragment) getSupportFragmentManager().findFragmentById(R.id.ux_fragment);

        arFragment.getPlaneDiscoveryController().hide();
        arFragment.getPlaneDiscoveryController().setInstructionView(null);
        arSceneView = arFragment.getArSceneView();
        ttpr = (TimeTableProviderContainer) getIntent().getSerializableExtra("ttpr");

        shared = getSharedPreferences("data", 0);
        device = shared.getString("device", "N/A");
        noise = shared.getString("noise", "N/A");
        voc = shared.getString("voc", "N/A");
        occurency = shared.getString("occurency", "N/A");
        room = shared.getString("room", "N/A");
        b = getIntent().getExtras();



        closestRoom = (TextView) findViewById(R.id.moreInfo);


        if(!PathFindingConstants.janitorMode){
            useCaseStudent();
        }else{
            useCaseJanitor();
        }



    }

    public void useCaseJanitor(){
        preRender(true);
        //showRoomInfo("janitor");
    }

    public void useCaseStudent(){
        try {
            isFree = ttpr.getTimeTableProvider().roomIsFree(b.getString("roomNr"));
            ttpr.getTimeTableProvider().getFreeRooms();
        } catch (Exception e) {
            Toast toast =
                    Toast.makeText(this, "Room not found.", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }

        if(!isFree){

            preRender(false);
            showRoomInfo("studentSearch");
        }else{
            showRoomInfo("studentFree");

        }
    }

    /**
     * Builds Arrow + Marker 3D Model
     */
    public void preRender(boolean janitor){
        ModelRenderable.builder()
                .setSource(this, Uri.parse("marker.sfb"))
                .build()
                .thenAccept(renderable -> {
                    markerRenderable = renderable;
                    ModelRenderable.builder()
                            .setSource(this, Uri.parse("pfeil2.sfb"))
                            .build()
                            .thenAccept(renderable2 -> {
                                arrowRenderable = renderable2;
                                createSimpleRoute(janitor);
                            })
                            .exceptionally(throwable -> {
                                Toast toast =
                                        Toast.makeText(this, "Unable to load marker", Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();
                                return null;
                            });

                    if (arrowRenderable == null) {
                        return;
                    }
                })
                .exceptionally(throwable -> {
                    Toast toast =
                            Toast.makeText(this, "Unable to load arrow", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    return null;
                });

        if (markerRenderable == null) {
            return;
        }
    }

    public void showRoom(View view) {
        showRoomInfo("janitor");
    }

    /**
     * Renders AR-Text Outputs
     */
    public void showRoomInfo(String type) {
        Vector3 forward;
        Vector3 cameraPosition;
        Vector3 position;
        Vector3 direction;


        forward = arSceneView.getScene().getCamera().getForward();
        cameraPosition = arSceneView.getScene().getCamera().getWorldPosition();
        position = Vector3.add(cameraPosition, forward);
        direction = Vector3.subtract(cameraPosition, position);
        direction.y = position.y;


        switch (type){
            case "janitor":
                ViewRenderable.builder()
                        .setView(this, R.layout.room_information)
                        .build()
                        .thenAccept(viewRenderable -> {



                            viewRenderable.getView().<TextView>findViewById(R.id.textViewRoomNameValue).setText(PathFindingConstants.freshAirRoom);
                            viewRenderable.getView().<TextView>findViewById(R.id.textViewNoiseValue).setText(noise + "%");
                            viewRenderable.getView().<TextView>findViewById(R.id.textViewOCCValue).setText(occurency);
                            viewRenderable.getView().<TextView>findViewById(R.id.textViewVOCValue).setText(voc + "%");


                            Vector3 dest = new Vector3(position.x, position.y + .75f, position.z - .75f);
                            info.setWorldPosition(dest);
                            info.setLookDirection(forward);
                            info.setRenderable(viewRenderable);
                            info.setParent(arSceneView.getScene());
                        });
                break;
            case "studentFree":
                showMore = (Button) findViewById(R.id.moreInfo);
                showMore.setVisibility(View.GONE);
                ViewRenderable.builder()
                        .setView(this, R.layout.room_info_free)
                        .build()
                        .thenAccept(viewRenderable1 -> {


                            Vector3 dest = new Vector3(position.x, position.y + .75f, position.z - .75f);
                            info.setWorldPosition(dest);
                            info.setLookDirection(forward);
                            info.setRenderable(viewRenderable1);
                            info.setParent(arSceneView.getScene());
                        });
                break;
            case "studentSearch":
                showMore = (Button) findViewById(R.id.moreInfo);
                showMore.setVisibility(View.GONE);
                freeRoomNr = searchClosestFreeRoom(b.getString("roomNr"));

                ViewRenderable.builder()
                        .setView(this, R.layout.room_info_full)
                        .build()
                        .thenAccept(viewRenderable2 -> {

                            viewRenderable2.getView().<TextView>findViewById(R.id.textViewNextValue).setText(freeRoomNr);

                            Vector3 dest = new Vector3(position.x, position.y + .75f, position.z - .75f);
                            info.setWorldPosition(dest);
                            info.setLookDirection(forward);
                            info.setRenderable(viewRenderable2);
                            info.setParent(arSceneView.getScene());
                        });
                break;
        }

    }

    private void createSimpleRoute(boolean janitor) {
        Vector3 forward;
        Vector3 cameraPosition;
        Vector3 position;
        Vector3 direction;
        Vector3 dest;

        float difference = 9;

        String arrowDirection = "right";
        Bundle b = getIntent().getExtras();

        String targetRoom = "107";

        if(janitor){
            targetRoom = "107"; //static value for showcase purpose
        }else{
            targetRoom = freeRoomNr;
        }

            try {
                difference =  (float) new DirectionProvider().provideWeightBetweenRooms(b.getString("roomNr"), targetRoom);
                Direction directionO = new DirectionProvider().provideDirectionFor(b.getString("roomNr"), targetRoom);
                if (directionO.isLeft()) {
                    arrowDirection = "left";
                }
                if (directionO.isRight()) {
                    arrowDirection = "right";
                }
                if (directionO.isBack()) {
                    arrowDirection = "back";
                }
                if (directionO.isFront()) {
                    arrowDirection = "front";
                }
                if (directionO.isHere()) {
                    arrowDirection = "here";
                }
                if (directionO.isUp()) {
                    arrowDirection = "up";
                }
                if (directionO.isDown()) {
                    arrowDirection = "down";
                }

            } catch (NoNeighbourNodeFoundExeption noNeighbourNodeFoundExeption) {
                showToast(noNeighbourNodeFoundExeption.getMessage());
            } catch (NoPathFoundExeption noPathFoundExeption) {
                showToast(noPathFoundExeption.getMessage());
            }


        forward = arSceneView.getScene().getCamera().getForward();
        cameraPosition = arSceneView.getScene().getCamera().getWorldPosition();
        position = Vector3.add(cameraPosition, forward);
        direction = Vector3.subtract(cameraPosition, position);
        direction.y = position.y;


        markerDestination.setLocalScale(new Vector3(.5f, .5f, .5f));
        arrowHere.setWorldPosition(position);
        arrowHere.setLookDirection(forward);

        switch (arrowDirection) {



            case "left": //LEFT

                dest = new Vector3(position.x - difference, position.y, position.z);

                markerDestination.setWorldPosition(dest);
                markerDestination.setLookDirection(arSceneView.getScene().getCamera().getLeft());
                markerDestination.setLocalRotation(Quaternion.axisAngle(new Vector3(0,1f,0 ), 315));

                break;
            case "right": //RIGHT

                Quaternion rotationArrowRight = Quaternion.axisAngle(new Vector3(0.0f, 0.0f, 1.0f), 180);
                arrowHere.setLocalRotation(rotationArrowRight);

                dest = new Vector3(position.x + difference, position.y, position.z);

                markerDestination.setWorldPosition(dest);
                markerDestination.setLookDirection(arSceneView.getScene().getCamera().getLeft());
                markerDestination.setLocalRotation(Quaternion.axisAngle(new Vector3(0, 1f, 0), 45));

                break;
            case "front": //FRONT

                arrowHere.setLocalRotation(Quaternion.axisAngle(new Vector3(0, 1f, 0), 270));

                dest = new Vector3(position.x, position.y, position.z - difference);

                markerDestination.setWorldPosition(dest);
                markerDestination.setLookDirection(arSceneView.getScene().getCamera().getLeft());
                markerDestination.setLocalRotation(Quaternion.axisAngle(new Vector3(0, 1f, 0), 75));
                break;
            case "back": //BACK


                arrowHere.setLocalRotation(Quaternion.axisAngle(new Vector3(0, 1f, 0), 90));


                dest = new Vector3(position.x, position.y, position.z + difference);

                markerDestination.setWorldPosition(dest);
                markerDestination.setLookDirection(arSceneView.getScene().getCamera().getLeft());

                break;


        }

        markerDestination.setRenderable(markerRenderable);
        markerDestination.setParent(arSceneView.getScene());

        arrowHere.setRenderable(arrowRenderable);
        arrowHere.setParent(arSceneView.getScene());
    }

    /**
     * @param startNodeId roomkey eg."002"
     * @return returns null/"<emty String>" in case of no free room found/ no Path found
     */
    private String searchClosestFreeRoom(String startNodeId) {
        try {
            return new DirectionProvider().provideClosestFreeRoom(startNodeId, ttpr.getTimeTableProvider().getFreeRooms());
        } catch (NoNeighbourNodeFoundExeption noNeighbourNodeFoundExeption) {
            noNeighbourNodeFoundExeption.printStackTrace();
        } catch (NoPathFoundExeption noPathFoundExeption) {
            noPathFoundExeption.printStackTrace();
        }
        showToast("Es wurde kein Raum/Weg gefunden.");
        return null;
    }

    private void showToast(String txt) {
        Toast.makeText(PathView.this, txt, Toast.LENGTH_LONG).show();
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(PathView.this, TextRecognitionActivity.class);
        intent.putExtra("ttpr", getIntent().getSerializableExtra("ttpr"));
        startActivity(intent);
    }

}
