package com.htwg.teamproject.arateamproject.helper;

import com.htwg.teamproject.arateamproject.helper.timeTableHelper.CustomTimeTableProvider;

import java.io.Serializable;

public class TimeTableProviderContainer implements Serializable {

    private CustomTimeTableProvider timeTableProvider;


    public CustomTimeTableProvider getTimeTableProvider() {
        return timeTableProvider;
    }

    public void setTimeTableProvider(CustomTimeTableProvider timeTableProvider) {
        this.timeTableProvider = timeTableProvider;
    }



}
