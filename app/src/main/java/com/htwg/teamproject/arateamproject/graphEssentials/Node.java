package com.htwg.teamproject.arateamproject.graphEssentials;

import java.util.ArrayList;
import java.util.List;

public class Node {

    private String name;
    private ArrayList<Edge> connections = new ArrayList<>();

    public Node(String name) {
        this.name = name;
    }

    void addConnection(Edge edge) {
        connections.add(edge);
    }

    public List<Edge> getConnections() {
        return connections;
    }

    public String getName() {
        return name;
    }

}
