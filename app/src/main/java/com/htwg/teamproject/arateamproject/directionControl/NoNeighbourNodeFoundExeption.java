package com.htwg.teamproject.arateamproject.directionControl;

public class NoNeighbourNodeFoundExeption extends Exception {

    public NoNeighbourNodeFoundExeption(String message) {
        super(message);
    }

}
