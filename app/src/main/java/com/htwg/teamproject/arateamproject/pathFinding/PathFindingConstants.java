package com.htwg.teamproject.arateamproject.pathFinding;

public class PathFindingConstants {

    public static final int ADDITIONAL_NODES = 5;

    //This is only for showcase & debugging

    public static String airFix = "100";
    public static String freshAirRoom = "107";

    public static boolean janitorMode = false;
}
